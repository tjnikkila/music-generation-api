# Midi generator

Amazing Grace is used to learn simple model to generate midi notes. 

**API Endpoint**:

```/midi/{useraction}```

_Parameters_ 
* useraction: integer between 1 and 4

_Returns_: 

```{"midi":Integer, "duration": Float, "velocity": Float, "time_to_next": Float}```

# Installation instructions

Tested by MacBookPro version 10.14.6 and python version 3.7.3.

1. Install Git large file storage https://git-lfs.github.com/
2. `git clone https://gitlab.com/tjnikkila/music-generation-api.git`
3. `cd music-generation-api`
4. Create virtual environment `python3 -m venv venv`
5. Activate virtual environment `source venv/bin/activate`
6. Install requirements `pip install -r requirements.txt`
7. Download Pytorch for your environment https://pytorch.org. Note that you probably have to change "pip3" to "pip" in your installation command.
8. Run application `FLASK_APP=backend.py flask run  --host=0.0.0.0 --port=5000`



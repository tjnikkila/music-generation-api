# NOTE: multithread unsafe! Front end takes care of syncronization.
# FLASK_APP=backend.py flask run  --host=0.0.0.0 --port=5000
from flask import Flask
from flask_cors import CORS

from flask import request
from flask import jsonify
import random
import time
import numpy as np
from multiprocessing import Process
# TODO: 
from vae_recurrent.vae_run import get_model_info
import torch
# TODO:  muuta perittyyn luokkaan
from data_util import IndependentNotes
import json
import random
from path_resolver import PathUtil

def init_episode():
    return {
        "events": [],
        "start_time": None
    }

app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})
dataloader = IndependentNotes(5)


path_util = PathUtil()
model, slot_info = get_model_info(path_util)

@app.route("/midi", methods=['GET'])
def prediction():
    global model
    global slot_info
    rnd_factor = 0.1
    randomnes = (random.random() - 0.5) * rnd_factor

    user_action = int(request.args.get('useraction'))
    hidden_val = slot_info[user_action - 1] + randomnes
    hidden_val = torch.tensor([hidden_val]).float()
    pred = model.generate_from_hidden(hidden_val)
    #pred = dataloader.denormalized_array(pred.unsqueeze(0))

    #midi, duration, velocity, time_to_next = pred.squeeze().detach().numpy()
    #print(int(midi), duration, velocity, time_to_next)

    return (jsonify(pred),
            200,
            {'Access-Control-Allow-Origin': '*'})

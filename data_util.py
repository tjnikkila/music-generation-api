import numpy as np


class Song:
    def __init__(self, batch_size):
        def max_min(vals):
            return vals.max(), vals.min()

        def normalize(vals, max_val, min_val):
            return (vals - min_val) / (max_val - min_val)

        def preprocess(vals):
            max_val, min_val = max_min(vals)
            normalized = normalize(vals, max_val, min_val)
            return normalized, max_val, min_val

        def calculate_time_to_next(song, tick_index):
            ticks = song_ts[:,tick_index]
            diffs = ticks[1:] - ticks[0:-1]

            return diffs

        song = np.array([62,1.5,0.75,0.41732283464566927,59,2.25,2.25,0.41732283464566927,67,2.25,1.5,0.4566929133858268,71,3.75,0.38124999999999964,0.5118110236220472,67,4.125,0.3546874999999998,0.4094488188976378,62,4.5,1.5296874999999996,0.4881889763779528,71,4.5,1.5296874999999996,0.5275590551181102,60,6,0.7117187499999993,0.47244094488188976,69,6,0.7117187499999993,0.4409448818897638,59,6.749999999999999,1.5296875000000005,0.44881889763779526,67,6.749999999999999,1.5296875000000005,0.4251968503937008,60,8.25,0.7117187499999993,0.49606299212598426,64,8.25,0.7117187499999993,0.4330708661417323,62,9,1.5,0.5196850393700787,62,10.5,0.75,0.4645669291338583,59,11.25,2.2499999999999982,0.44881889763779526,67,11.25,1.5,0.5275590551181102,71,12.75,0.38124999999999964,0.5039370078740157,67,13.125,0.35468749999999893,0.4645669291338583,62,13.499999999999998,1.5,0.5039370078740157,71,13.499999999999998,1.5,0.5354330708661418,60,14.999999999999998,0.75,0.6377952755905512,69,14.999999999999998,0.75,0.6299212598425197,62,15.749999999999998,3.7500000000000018,0.8110236220472441,74,15.749999999999998,3.7500000000000018,0.8582677165354331,62,19.5,0.75,0.7637795275590551,71,19.5,0.75,0.6771653543307087,62,20.25,2.25,0.7874015748031497,74,20.25,1.125,0.8740157480314961,71,21.375,0.375,0.7165354330708661,74,21.75,0.3812500000000014,0.84251968503937,71,22.125,0.3546875000000007,0.6692913385826772,59,22.5,1.5,0.7165354330708661,67,22.5,1.5,0.6929133858267716,62,24,0.75,0.8503937007874016,60,24.75,2.25,0.7401574803149606,64,24.75,1.125,0.6692913385826772,67,25.875,0.375,0.7244094488188977,67,26.25,0.3812500000000014,0.7086614173228346,64,26.625,0.3546875000000007,0.5433070866141733,62,27,1.5,0.6456692913385826,62,28.5,0.75,0.5669291338582677,59,29.25,2.25,0.4645669291338583,67,29.25,1.5,0.5196850393700787,71,30.75,0.3812500000000014,0.5433070866141733,67,31.125,0.3546875000000007,0.4330708661417323,62,31.5,1.5,0.5354330708661418,71,31.5,1.5,0.5511811023622047,60,33,0.75,0.4566929133858268,69,33,0.75,0.48031496062992124,60,33.75,2.293750000000003,0.5039370078740157,67,33.75,4.500000000000007,0.48031496062992124,59,36,2.1359375000000043,0.36220472440944884])

        song_ts = np.reshape(song, (-1, 4))

        diffs = calculate_time_to_next(song_ts, 1)

        song_ts = np.c_[song_ts[:-1,:], diffs]
        song_ts = np.delete(song_ts, [1], 1)
        self.origin = song_ts.copy()

        self.midi_index = 0
        self.duration_index = 1
        self.velocity_index = 2
        self.time_next_note_index = 3

        song_ts[:,self.midi_index], self.max_midi, self.min_midi = preprocess(song_ts[:,self.midi_index])
        song_ts[:,self.duration_index], self.max_duration, self.min_duration = preprocess(song_ts[:,self.duration_index])
        song_ts[:,self.velocity_index], self.max_velocity, self.min_velocity = preprocess(song_ts[:,self.velocity_index])
        song_ts[:,self.time_next_note_index], self.max_time_next_note, self.min_time_next_note = preprocess(song_ts[:,self.time_next_note_index])

        self.song_ts = song_ts
        self.batch_size = batch_size

    def denormalized_array(self, vals):
        def denorm(vals, max_val, min_val):
            return vals * (max_val - min_val) + min_val

        vals[:,self.midi_index] = denorm(vals[:,self.midi_index], self.max_midi, self.min_midi)
        vals[:,self.duration_index] = denorm(vals[:,self.duration_index], self.max_duration, self.min_duration)
        vals[:,self.velocity_index] = denorm(vals[:,self.velocity_index], self.max_velocity, self.min_velocity)
        vals[:,self.time_next_note_index] = denorm(vals[:,self.time_next_note_index], self.max_time_next_note, self.min_time_next_note)
        return vals


class IndependentNotes(Song):
    def __init__(self, batch_size):
        Song.__init__(self, batch_size)
        self.samples_taken = 0

    def reset_iterator(self):
        self.samples_taken = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.samples_taken > self.song_ts.shape[0]:
            raise StopIteration

        self.samples_taken += self.batch_size
        idx = np.random.randint(self.song_ts.shape[0], size=self.batch_size)
        return self.song_ts[idx]

class DependentNotes(Song):
    def __init__(self, batch_size):
        Song.__init__(self, batch_size)
        self.samples_taken = 0
        self.next_index = 0

    def reset_iterator(self):
        self.samples_taken = 0

    def __iter__(self):
        return self

    def __next__(self):
        self.next_index += 1

        if self.next_index >= self.song_ts.shape[0] - self.batch_size:
            self.next_index = 0
            raise StopIteration
    
        return self.song_ts[self.next_index:self.next_index+self.batch_size]


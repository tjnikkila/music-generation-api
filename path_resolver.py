from pathlib import Path


class PathUtil:
    def __init__(self):
        working_directory = Path(__file__).resolve().parent.as_posix()
        self.user_action_idx_file = working_directory + "/vae_recurrent/model_parameters/user_action_idx.npy"
        self.mean_hidden_file = working_directory + "/vae_recurrent/model_parameters/mean_hidden.npy"
        self.save_model_to = working_directory + "/vae_recurrent/model_parameters/model.pt"

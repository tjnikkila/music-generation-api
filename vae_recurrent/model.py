import torch
import torch.nn as nn
import pyro
import numpy as np
import pyro.distributions as dist


class Decoder(nn.Module):
    def __init__(self, z_dim, hidden_dim1, hidden_dim2, hidden_dim3, x_dim):
        super(Decoder, self).__init__()

        # setup the two linear transformations used
        hidden_dim = hidden_dim1
        self.fc1 = nn.Linear(z_dim+1, hidden_dim)
        self.fc21 = nn.Linear(hidden_dim1, hidden_dim2)
        self.fc22 = nn.Linear(hidden_dim2, hidden_dim3)
        out_dim = hidden_dim3
        self.fc31 = nn.Linear(out_dim, x_dim)
        self.fc32 = nn.Linear(out_dim, x_dim)
        self.dropout = nn.Dropout(p=0.3)
        
        # setup the non-linearities
        self.softplus = nn.Softplus()
        self.hidden_activation = nn.LeakyReLU()
        #self.sigmoid = nn.Sigmoid()
        #self.lrelu = nn.LeakyReLU()
        self.activation31 = nn.LeakyReLU()
        
    def forward(self, z):
        # define the forward computation on the latent z
        # first compute the hidden units
        hidden = self.hidden_activation(self.fc1(z))
        #hidden = self.dropout(hidden)
        hidden = self.hidden_activation(self.fc21(hidden))
        #hidden = self.dropout(hidden)
        hidden = self.hidden_activation(self.fc22(hidden))
        # return the parameter for the output Bernoulli
        # each is of size batch_size x 784
        z_loc = self.activation31(self.fc31(hidden))
        z_scale = torch.exp(self.fc32(hidden))
        return z_loc, z_scale

class Encoder(nn.Module):
    def __init__(self, z_dim, hidden_dim1, hidden_dim2, hidden_dim3, x_dim):
        super(Encoder, self).__init__()
        # setup the three linear transformations used
        self.fc1 = nn.Linear(x_dim, hidden_dim1)
        self.fc21 = nn.Linear(hidden_dim1, hidden_dim2)
        self.fc22 = nn.Linear(hidden_dim2, hidden_dim3)
        
        hidden_dim = hidden_dim3
        self.fc31 = nn.Linear(hidden_dim, z_dim)
        self.fc32 = nn.Linear(hidden_dim, z_dim)
        self.dropout = nn.Dropout(p=0.3)
        # setup the non-linearities
        self.softplus = nn.LeakyReLU()
        self.hidden_activation = nn.LeakyReLU()
        #self.lrelu = nn.LeakyReLU()
        
    def forward(self, x):
        hidden = self.hidden_activation(self.fc1(x))
        #hidden = self.dropout(hidden)

        hidden = self.hidden_activation(self.fc21(hidden))
        #hidden = self.dropout(hidden)
        hidden = self.hidden_activation(self.fc22(hidden))
        z_loc = self.fc31(hidden)
        z_scale = torch.exp(self.fc32(hidden))
        #cosntraint = constraints.unit_interval #lower_cholesky
        #z_loc = transform_to(cosntraint)(z_loc)
        #z_scale = transform_to(cosntraint)(z_scale)
        return z_loc, z_scale

class VaeHelper:
    
    hidden_dim1 = 8
    hidden_dim2 = 15
    hidden_dim3 = 8
    x_dim = 4

class VAE(nn.Module):
    def __init__(self, z_dim, device, use_cuda=False, hidden_dim1=VaeHelper.hidden_dim1, hidden_dim2=VaeHelper.hidden_dim2, hidden_dim3=VaeHelper.hidden_dim3, x_dim = VaeHelper.x_dim):
        super(VAE, self).__init__()

        self.encoder = Encoder(z_dim, hidden_dim1, hidden_dim2, hidden_dim3, x_dim)
        self.decoder = Decoder(z_dim, hidden_dim1, hidden_dim2, hidden_dim3, x_dim)
        if use_cuda:
            self.encoder = self.encoder.cuda()
            self.decoder = self.decoder.cuda()

        self.z_dim = z_dim
        self.looppi = 0
        self.previous_hidden = None
        self.z_loc = None
        self.z_scale = None
        self.z_quide = None
        self.device = device

    # define the model p(x|z)p(z)
    def model(self, x):
        self.looppi += 1
        # register PyTorch module `decoder` with Pyro
        pyro.module("decoder", self.decoder)
        with pyro.plate("data", x.shape[0]):
            # setup hyperparameters for prior p(z)
            #z_loc = pyro.param("z_loc_m", x.new_zeros(torch.Size((x.shape[0], self.z_dim))), constraint=torch.distributions.constraints.positive)
            z_loc = pyro.param("z_loc_m", x.new_zeros(torch.Size((x.shape[0], self.z_dim))))
            z_scale = pyro.param("z_scale_m", x.new_ones(torch.Size((x.shape[0], self.z_dim))), constraint=torch.distributions.constraints.positive)
            
            z = pyro.sample("latent", dist.Normal(z_loc, z_scale).to_event(1))
            
            if self.previous_hidden is None:
                self.previous_hidden = z.cpu().detach().numpy()
                
            
            #x_loc = pyro.param("x_loc_m", x.new_zeros(torch.Size((x.shape[0], self.z_dim))), constraint=torch.distributions.constraints.real)
            #x_scale = pyro.param("x_scale_m", x.new_ones(torch.Size((x.shape[0], self.z_dim))), constraint=torch.distributions.constraints.positive)
            
            third_tensor = torch.cat((z, torch.tensor(self.previous_hidden, device=self.device)), 1)

            x_loc, x_scale = self.decoder.forward(third_tensor)
            # score against actual images

            if self.looppi % 20000 == 0:
                print(self.looppi)

            self.previous_hidden = z.cpu().detach().numpy()

            #m = MultivariateNormal(torch.zeros(2), torch.eye(2))
            #pyro.sample("obs", dist.Normal(x_loc, x_scale).to_event(1), obs=x)
            pyro.sample("obs", dist.Normal(x_loc, x_scale).to_event(1), obs=x)

    # define the guide (i.e. variational distribution) q(z|x)
    def guide(self, x):
        # register PyTorch module `encoder` with Pyro
        pyro.module("encoder", self.encoder)
        with pyro.plate("data", x.shape[0]):
            # use the encoder to get the parameters used to define q(z|x)

            z_loc, z_scale = self.encoder.forward(x)
      
            if self.looppi % 30000 == 0 and self.z_loc is not None:
              print("z_loc diff")
              print(self.z_loc.cpu().detach().numpy() -  z_loc.cpu().detach().numpy())
          
            if np.isnan(z_loc.sum().cpu().detach()):
              print("current z_loc")
              print(z_loc)
              print("prev z_loc")
              print(self.z_loc)
              print("prev z_scale")
              print(self.z_scale)
            
            assert not np.isnan(z_loc.sum().cpu().detach())
            assert not np.isnan(z_scale.sum().cpu().detach())

            self.z_loc, self.z_scale = z_loc, z_scale
            
            z = pyro.sample("latent", dist.Normal(z_loc, z_scale).to_event(1))
            

            return z

    def reconstruct_img(self, x, device, previous_hidden):
        # encode image x
        z_loc, z_scale = self.encoder(x)
        # sample in latent space
        z = dist.Normal(z_loc, z_scale).to_event(1).sample()
        #z_full = torch.cat((z, z), 1)
        # decode the image (note we don't sample in image space)
        if previous_hidden is None:
          previous_hidden = z.cpu().detach().numpy()
        third_tensor = torch.cat((z, torch.tensor(previous_hidden, device=device)), 1)
        x_loc, x_scale = self.decoder(third_tensor)
        return dist.Normal(x_loc, x_scale).sample(), x_loc.cpu().detach().numpy(), x_scale.cpu().detach().numpy(), previous_hidden, z.cpu().detach().numpy(), z_loc.cpu().detach().numpy()
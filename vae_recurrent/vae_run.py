import torch
import pyro
import pyro.distributions as dist
import torch.nn as nn
import numpy as np
from flask import jsonify

from data_util import DependentNotes
from vae_recurrent.model import Decoder, VaeHelper
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

class PyroVaeModel:
    def __init__(self, path_util):
        z_dim = 1
        checkpoint = torch.load(path_util.save_model_to)  
        self.decoder = Decoder(z_dim, hidden_dim1=VaeHelper.hidden_dim1, hidden_dim2=VaeHelper.hidden_dim2, hidden_dim3=VaeHelper.hidden_dim3, x_dim = VaeHelper.x_dim)
        self.decoder.load_state_dict(checkpoint['decoder'])

        self.previous_hidden = None

    def generate_from_hidden(self, hidden_val):

        if self.previous_hidden is None:
            self.previous_hidden = hidden_val

        third_tensor = torch.tensor([hidden_val[0], self.previous_hidden[0]])
        x_loc, x_scale = self.decoder(third_tensor)

        pred = dist.Normal(x_loc, x_scale).sample()
        pred = DependentNotes(1).denormalized_array(pred.unsqueeze(0))
        midi, duration, velocity, time_to_next = pred.squeeze().detach().numpy()
        print(int(midi), duration, velocity, time_to_next)

        self.previous_hidden = hidden_val
        return {"midi":int(midi), "duration": duration.item(), "velocity": velocity.item(), "time_to_next": time_to_next.item()}

def get_model_info(path_util):
    slot_info = np.load(path_util.user_action_idx_file, allow_pickle=True)
    return PyroVaeModel(path_util), slot_info

import torch.nn as nn
import pyro
from pyro.infer import SVI, Trace_ELBO
from pyro.optim import Adam
import torch
pyro.enable_validation(True)
pyro.distributions.enable_validation(False)
from torch.distributions import constraints, transform_to
import numpy as np
from data_util import DependentNotes
from vae_recurrent.model import VAE
from path_resolver import PathUtil
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

def train(svi, train_loader, use_cuda=False):
    epoch_loss = 0.
    # do a training epoch over each mini-batch x returned
    # by the data loader
    for x in train_loader:
        x = torch.tensor(x, device=device).float()
        # if on GPU put mini-batch into CUDA memory
        if use_cuda:
            x = x.cuda()

        epoch_loss += svi.step(x)

    total_epoch_loss_train = epoch_loss
    return total_epoch_loss_train

def get_sampled_hiddens_and_mean_hiddens(vae, train_loader, device):
    train_loader.reset_iterator()
    prev_hidden = None
    sampled_hidden = []
    mean_hidden = []
    for x in train_loader:
        prev_hidden = None
        x_new = x
        x_new = torch.tensor(x_new, device=device).float()
        for i in range(5):
            x_rec, x_loc, x_scale, prev_hidden, z, z_loc = vae.reconstruct_img(x_new, device, prev_hidden)
            sampled_hidden.append(z)
            if i == 0:
                mean_hidden.append(z_loc)

    sampled_hidden = np.array(sampled_hidden)
    sampled_hidden = sampled_hidden.flatten()
    sampled_hidden.sort()
    sampled_hidden = np.around(sampled_hidden, decimals=1)

    return sampled_hidden, np.array(mean_hidden)
def train_model():
    batch_size = 1
    train_loader = DependentNotes(batch_size)
    z_dim = 1
    USE_CUDA = False
    # Run only for a single iteration for testing
    LEARNING_RATE = 5.0e-4
    TEST_FREQUENCY = 20000
    path_util = PathUtil()
    adam_args = {"lr": LEARNING_RATE, "betas": (0.95, 0.99)}
    batch_size = 8


    # clear param store
    pyro.clear_param_store()
    vae = VAE(z_dim, device, USE_CUDA)
    train_loader = DependentNotes(batch_size)
    optimizer = Adam(adam_args)
    svi = SVI(vae.model, vae.guide, optimizer, loss=Trace_ELBO())
    vae.decoder.train()
    vae.encoder.train()
    NUM_EPOCHS = 850

    # training loop
    for epoch in range(NUM_EPOCHS):
        train_loader.reset_iterator()
        total_epoch_loss_train = train(svi, train_loader, use_cuda=USE_CUDA)
        if epoch % 20 == 0:
            print("[epoch %03d]  average training loss: %.4f" % (epoch, total_epoch_loss_train))

    torch.save({
                'epoch': NUM_EPOCHS,
                'encoder': vae.encoder.state_dict(),
                'decoder': vae.decoder.state_dict(),
                'optimizer_state_dict': optimizer.get_state()
                }, path_util.save_model_to)

    vae_eval = VAE(z_dim, USE_CUDA)
    optimizer = Adam(adam_args)

    # Test load works
    checkpoint = torch.load(path_util.save_model_to)
    vae_eval.encoder.load_state_dict(checkpoint['encoder'])
    vae_eval.decoder.load_state_dict(checkpoint['decoder'])
    optimizer.set_state(checkpoint['optimizer_state_dict'])

    vae_eval.decoder.eval()
    vae_eval.encoder.eval()

    # Go through data and reconstruc inpu.
    # Save sampled hidden value of all 5 samples of all data.
    # Save mean value of one hidden value of all data.
    sampled_hidden, mean_hidden = get_sampled_hiddens_and_mean_hiddens(vae, train_loader, device)
    sampled_hidden.sort()
    user_action_idx = []
    for i in range(1,5):
        index = sampled_hidden.shape[0] // 5 * i
        user_action_idx.append(sampled_hidden[index])

    user_action_idx = np.array(user_action_idx)
    mean_hidden = mean_hidden[:,0,:].squeeze()

    np.save(path_util.user_action_idx_file, user_action_idx)
    np.save(path_util.mean_hidden_file, mean_hidden)
